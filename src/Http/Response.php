<?php
namespace Mauro\Wordpress\Http;

class Response {
    private $attributes = [];

    public function __construct(array $data) {
        $this->set($data);
    }

    public function set(array $data) {
        $this->attributes = $data;
    }

    public static function json(array $array) {
        wp_send_json($array);
    }

    public static function redirect($url, array $options = []) {
        foreach ($options as $key => $option) {
            $_SERVER[$key] = $option;
        }

        wp_redirect($url);
    }

    public function toArray() {
        return $this->attributes;
    }

    public function toJson() {
        return json_encode($this->toArray());
    }

    public function send() {
        self::json($this->toArray());
    }
}
