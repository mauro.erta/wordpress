<?php
namespace Mauro\Wordpress\Http;

use Mauro\Wordpress\Http\Response;
use Mauro\Wordpress\Validation\Validator;

class Request {
    protected $attributes, $files, $is_ajax;

    public function __construct() {
        // $_REQUEST ha anche i COOKIE, non sono sicuro di volerli
        // $this->attributes = $_REQUEST;
        $this->attributes = array_merge($_GET, $_POST);
        $this->files = $_FILES;
        $this->is_ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

        if(!$this->authorize()) {
            if($this->is_ajax) {
                return Response::json([
                    'success' => false,
                    'status' => 403,
                    'message' => 'You are not authorize'
                ]);
            } else {
                return Response::redirect('', [
                    'status' => 403,
                    'message' => 'You are not authorize'
                ]);
            }
        }

        $this->validate();
    }

    public function validate() {
        $validator = new Validator($this->rules(), $this->all(true));
        $errors = $validator->validate();

        if(!empty($errors)) {
            if($this->is_ajax) {
                return Response::json([
                    'success' => false,
                    'status' => 422,
                    'errors' => $errors,
                    'request' => $this->all()
                ]);
            } else {
                return Response::redirect('', [
                    'status' => 422,
                    'errors' => $errors
                ]);
            }
        }
    }

    public function authorize() {
        return true;
    }

    public function rules() {
        return [];
    }

    public function __get($key) {
        return $this->get($key);
    }

    public function ajax() {
        return $this->is_ajax;
    }

    public function has($key) {
        return isset($this->attributes[$key]);
    }

    public function get($key) {
        return $this->has($key) ? $this->attributes[$key] : null;
    }

    public function hasFile($key) {
        return isset($this->files[$key]);
    }

    public function file($key) {
        return $this->hasFile($key) ? $this->files[$key] : null;
    }

    public function all($files = false) {
        return $files ? array_merge($this->attributes, $this->files) : $this->attributes;
    }

    public function only($fields = []) {
        if(!is_array($fields))
            $fields = [$fields];
        $response = [];
        foreach ($fields as $field) {
            $response[$field] = $this->get($field);
        }
        return $response;
    }

    public function except($fields = []) {
        if(!is_array($fields))
            $fields = [$fields];
        $response = $this->all();
        foreach ($fields as $field) {
            if(isset($response[$field]))
                unset($response[$field]);
        }
        return $response;
    }
}
