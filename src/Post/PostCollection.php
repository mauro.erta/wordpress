<?php
namespace Mauro\WordPress\Post;

use Iterator;
use ArrayAccess;

use Mauro\Wordpress\Post\Post;

class PostCollection implements Iterator, ArrayAccess {
    private $posts = [],
            $position;

    public function __construct($posts) {
        if(!is_array($posts)) $posts = [$posts];
        $this->posts = array_filter($posts, function($post) {
            return $post instanceof Post;
        });
        $this->position = 0;
    }

    public function current() { return $this->posts[$this->key()]; }
    public function next() { $this->position++; }
    public function rewind() { $this->position = 0; }
    public function key() { return $this->position; }
    public function valid() { return isset($this->posts[$this->key()]); }
    public function reverse() {
        $this->posts = array_reverse($this->posts);
        $this->rewind();
    }
    public function clear() {
        $this->position = 0;
        $this->posts = [];
    }

    public function copy() { return new self($this->posts); }
    public function isEmpty() { return empty($this->posts); }

    public function offsetExists ($offset) { return isset($this->posts[$offset]); }
    public function offsetGet ($offset) { return $this->get($offset); }
    public function offsetSet ($offset, $value) { $this->posts[$offset] = $value; }
    public function offsetUnset ($offset) { unset($this->posts[$offset]); }

    public function toArray() {
        return array_map(function($post) {
            return $post->toArray();
        }, $this->posts);
    }

    public function toJson() {
        return json_encode($this->toArray());
    }

    public function add(Post $post) {
        return $this->posts[$this->position++] = $post;
    }

    public function delete($key = null) {
        $key = $key ? $key : $this->key();
        if(isset($this->posts[$key]))
            unset($this->posts[$key]);
        else throw new KeyInvalidException("Invalid key $key.");
    }

    public function get($key) {
        $key = $key ? $key : $this->key();
        if(isset($this->posts[$key]))
            return $this->posts[$key];
        else throw new KeyInvalidException("Invalid key $key.");
    }

    public function keys() {
        return array_keys($this->posts);
    }

    public function count() {
        return count($this->posts);
    }

    public static function create($posts = []) {
        return new self($posts);
    }
}
