<?php
namespace Mauro\Wordpress\Validation;

use Mauro\Wordpress\Http\Response;

class Validator {
    protected $rules, $fields;
    protected $conditions = ['required', 'string', 'max', 'min', 'in', 'not_in', 'file', 'extension', 'size'];

    public function __construct(array $rules = [], array $fields = []) {
        $this->rules = $rules;
        $this->fields = $fields;
    }

    public function check($field, $rule) {
        $conditions = explode('|', $rule);
        $checks = [];
        foreach ($conditions as $key => $condition) {
            $name = explode(':', $condition)[0];

            if(in_array($name, $this->conditions)) {
                $method = 'check' . ucfirst(strtolower($name)) . 'Condition';
                if(method_exists($this, $method) && isset($this->fields[$field])) {
                    return $this->{$method}($field, $condition);
                }
            }
        }

        return !in_array('required', $conditions);
    }

    public function getErrorMessage($field, $condition) {
        return "Error on $field field: [$condition] condition not respected";
    }

    public function validate() {
        if(empty($this->rules))
            return [];

        $errors = [];
        foreach ($this->rules as $field => $rules) {
            $rules = explode('|', $rules);
            foreach ($rules as $key => $rule) {
                if(!$this->check($field, $rule)) {
                    $errors[$field] = !is_array($errors[$field]) ? [] : $errors[$field];
                    $errors[$field][] = $this->getErrorMessage($field, $rule);
                }
            }
        }

        return $errors;
    }

    public function checkRequiredCondition($field) {
        return isset($this->fields[$field]) && $this->fields[$field] != null && $this->fields[$field] != '';
    }

    public function checkStringCondition($field) {
        return is_string($this->fields[$field]);
    }

    public function checkMaxCondition($field, $condition) {
        if(count(explode(':', $condition)) < 2) return true;
        $max = explode(':', $condition)[1];
        return count($this->fields[$field]) < $max;
    }

    public function checkMinCondition($field, $condition) {
        if(count(explode(':', $condition)) < 2) return true;
        $min = explode(':', $condition)[1];
        return count($this->fields[$field]) > $min;
    }

    public function checkInCondition($field, $condition) {
        if(count(explode(':', $condition)) < 2) return true;
        $list = explode(':', $condition)[1];
        $array = explode(',', $list);
        return in_array($this->fields[$field], $array);
    }

    public function checkNot_inCondition($field, $condition) {
        if(count(explode(':', $condition)) < 2) return true;
        $list = explode(':', $condition)[1];
        $array = explode(',', $list);
        return !in_array($this->fields[$field], $array);
    }

    public function checkFileCondition($field, $condition = null) {
        if(!isset($this->fields[$field]['tmp_name'])) return false;
        if(!is_array($this->fields[$field]['tmp_name'])) return is_file($this->fields[$field]['tmp_name']);

        foreach ($this->fields[$field]['tmp_name'] as $key => $file) {
            if(!is_file($file)) return false;
        }

        return true;
    }

    public function checkSizeCondition($field, $condition) {
        $size = explode(':', $condition)[1];
        if(!$this->checkFileCondition($field)) return false;

        foreach ($this->fields[$field]['tmp_name'] as $key => $file) {
            if(filesize($file) >= ($size * 1000))
                return false;
        }

        return true;
    }

    public function checkExtensionCondition($field, $condition) {
        if(!$this->checkFileCondition($field, $condition)) return false;
        if(count(explode(':', $condition)) < 2) return true;
        $list = explode(':', $condition)[1];
        $extensions = explode(',', $list);

        foreach ($this->fields[$field]['name'] as $key => $file_name) {
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            $ext = strtolower($ext);
            if(!in_array($ext, $extensions)) return false;
        }

        return true;
    }
}
