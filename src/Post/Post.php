<?php
namespace Mauro\WordPress\Post;

use WP_Query;
use Mauro\Wordpress\Post\PostCollection;

class Post {
    protected $attributes;

    public function __construct(array $post = []) {
        $this->attributes = $post;
    }

    public function __get($name) {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    public function __set($name, $value) {
        return $this->attributes[$name] = $value;
    }

    public function toArray() {
        return $this->attributes;
    }

    public function save() {
        $args = $this->attributes;

        if(!$this->ID) unset($args['ID']);

        $post_id = wp_insert_post($args);

        if(is_wp_error($post_id))
            return false;

        return true;
    }

    public static function create($args) {
        $post = new self($args);
        return $post->save() ? $post : false;
    }

    public static function find($id) {
        return self::query(['p' => $id])[0];
    }

    public static function all() {
    	return self::query();
    }

    public static function take($num = 8) {
        return self::query(['post_per_page' => $num]);
    }

    private static function query(array $args = [], $return_objects = true) {
        $args['post_type'] = isset($args['post_type']) ? $args['post_type'] : 'post';
        $args['posts_per_page'] = isset($args['posts_per_page']) ? $args['posts_per_page'] : -1;
        $args['post_status'] = isset($args['post_status']) ? $args['post_status'] : 'any';
        $args['orderby'] = isset($args['orderby']) ? $args['orderby'] : 'date';
        $args['order'] = isset($args['order']) ? $args['order'] : 'ASC';
        $query = new WP_Query($args);

        if(!$return_objects)
            return $query->posts;

        $posts = [];
        foreach($query->posts as $key => $wp_post) {
            $post = new self([
                'ID' => $wp_post->ID,
                'post_title' => $wp_post->post_title,
                'post_content' => $wp_post->post_content,
                'post_date' => $wp_post->post_date,
                'post_author' => get_user_by('id', $wp_post->post_author)
            ]);

            $posts[] = $post;
        }

        return new PostCollection($posts);
    }
}
